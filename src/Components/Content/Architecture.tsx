import React from "react";
import logo from '../../Assets/logo.svg';
import '../../Styles/Unified/main.min.css';

const Architecture: React.FC = () => {
    return(
        <div className={'Architecture'}>
            <img src={logo} className={'Splash-logo'} alt={logo}/>
            <ul>
                <li>
                    Architecture
                </li>
            </ul>
        </div>
    )
}

export default Architecture;