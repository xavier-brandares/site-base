import React from "react";
import logo from '../../Assets/logo.svg';
import '../../Styles/Unified/main.min.css';

const HouseInTheTwentyFirstCentury: React.FC = () => {
    return(
        <div className={'Splash'}>
            <img src={logo} className={'Splash-logo'} alt={logo}/>
            <ul>
                <li>
                    House In The Twenty-First Century
                </li>
            </ul>
        </div>
    )
}

export default HouseInTheTwentyFirstCentury;